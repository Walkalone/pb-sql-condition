'use strict'
const LIKE_ANY_CHAR = '%'
const LIKE_ONE_CHAR = '_'

const NOT_SYMB = '!'
const NOT_OR_SYMB = '~'
const EXCLUDE_SYMB = '^'

const STAR_SYMB = '*'
const QUESTION_SYMB = '?'
const SEQUENCE_SYMB = ';'
const EMPTY_SYMB = '()'
const INTERVAL_SYMB = '::'

const PARAM_SYMB = ':'
const PARAM_PREF = 'P'
const PARAM_NAME_LEN = 4

function replaceLikeSymbs (s) {
  // замена * на %
  s = s.replace(new RegExp('\\' + STAR_SYMB, 'g'), LIKE_ANY_CHAR)
  s = s.replace(new RegExp('\\' + QUESTION_SYMB, 'g'), LIKE_ONE_CHAR)
  // избавляемся от возможных %%
  const doubleLike = new RegExp(LIKE_ANY_CHAR + LIKE_ANY_CHAR, 'g')
  do {
    s = s.replace(doubleLike, LIKE_ANY_CHAR)
  } while (s.search(doubleLike) >= 0)
  // кавычка в две кавычки
  s = s.replace(new RegExp(`'`, 'g'), `''`)
  return s

}

class Conditions {
  /**
   * SQL Query condition builder
   * @param {boolean} [ignoreCase=false] optional flag for case insensitivity
   */
  constructor (ignoreCase = false) {
    this._ignoreCase = ignoreCase
    this._wheres = []
    this._params = []
  }

  _genParamName () {
    return PARAM_PREF + String(this._params.length + 1).padStart(PARAM_NAME_LEN - PARAM_PREF.length,'0')
  }

  _addParam (val, type) {
    const name = this._genParamName()
    if (type === 's' && this._ignoreCase) val = val.toUpperCase()
    this._params.push({
      name,
      type,
      val
    })
    return PARAM_SYMB + name
  }

  /**
   * Add a condition clause for the column on the masked string
   * @param {string} column Column to compare
   * @param {string} condition Masked string of condition
   * @param {int} [pad=0] Left pad value to
   */
  addCode (column, condition, pad = 0) {
    this._wheres.push(this._getCodeCondition(column, condition, pad))
  }

  getWhere (wrapAnd = true) {
    let result = this._wheres.filter(a => a).join(`\n${wrapAnd ? '  ' : ''}and `)
    if (result) {
      return wrapAnd ? '\nand (\n  ' + result + '\n)' : result
    }
    else return ''
  }

  getParams () {
    return this._params
  }

  /**
   * Generates a condition clause for the column on the masked string
   * @param {string} column Column to compare
   * @param {string} condition Masked string of condition
   * @param {int} pad Left pad value to
   * @return {string}
   */
  _getCodeCondition (column, condition, pad) {
    const hasPref = new RegExp('^[' + NOT_SYMB + NOT_OR_SYMB + EXCLUDE_SYMB + ']')
    const hasLike = new RegExp('[' + LIKE_ANY_CHAR + LIKE_ONE_CHAR + ']', 'g')

    if (!condition) return ''
    let lColumn = column
    if (this._ignoreCase) {
      lColumn = 'upper(' + lColumn + ')'
    }
    let lColumnTrimmed = lColumn
    if (pad) lColumnTrimmed = 'ltrim(' + lColumnTrimmed + ')'


    // замена * -> % и ? -> _
    condition = replaceLikeSymbs(condition)

    // разбивам на части по ';'
    let parts = condition.split(SEQUENCE_SYMB)
    let lCondsNorm = []
    let lCondsNot = []
    let lCondsNotOr = []
    let lCondsExclude = []
    let lCond, lPref
    // формируем условия для часетй
    for (let i = 0; i < parts.length; i++) {
      if (!parts[i]) continue
      lCond = {
        not: false,
        start: '',
        end: '',
        empty: false,
        like: false,
        interval: false
      }
      // проверяем есть ли отрицающий префикс
      if (parts[i].search(hasPref) === 0) {
        lCond.not = true
        lPref = parts[i][0]
        lCond.start = parts[i].substr(1)
      }
      else {
        lPref = ''
        lCond.start = parts[i]
      }
      lCond.empty = (lCond.start === EMPTY_SYMB)
      // проверяем нужен ли like
      lCond.like = lCond.start.search(hasLike) >= 0
      if (!lCond.like) {
        // проверяем задан ли диапазон
        const intervals = lCond.start.split(INTERVAL_SYMB)
        lCond.start = intervals[0].padStart(pad)
        if (intervals.length > 1) {
          lCond.end = intervals[intervals.length - 1].padStart(pad)
          lCond.interval = true
        }
      }
      if (lCond.start === LIKE_ANY_CHAR) continue
      switch (lPref) {
        case NOT_SYMB:
          lCondsNot.push(lCond)
          break
        case NOT_OR_SYMB:
          lCondsNotOr.push(lCond)
          break
        case EXCLUDE_SYMB:
          lCondsExclude.push(lCond)
          break
        default:
          lCondsNorm.push(lCond)
          break
      }
    }
    const make = c => {
      if (c.empty) {
        return `${column} is${c.not ? ' not ' : ' '}null`
      }
      if (c.like) {
        return `${lColumnTrimmed}${c.not ? ' not ' : ' '}like ${this._addParam(c.start, 's')}`
      }
      if (!c.interval) return `${lColumn} ${c.not ? '<>' : '='} ${this._addParam(c.start, 's')}`
      let ints = []
      if (c.start) ints.push(`${lColumn} >= ${this._addParam(c.start, 's')}`)
      if (c.end) ints.push(`${lColumn} <= '${this._addParam(c.end, 's')}'`)
      return `${c.not ? 'not ' : ''}(${ints.join(' and ')})`
    }
    const whereNorm = lCondsNorm.map(make).join(' or ')
    const whereNot = lCondsNot.map(make).join(' and ')
    const whereNotOr = lCondsNotOr.map(make).join(' or ')
    const whereExclude = lCondsExclude.map(make).join(' and ')

    let result = whereNorm
    if (whereNot) {
      result += result ? ' and (' + whereNot + ')' : whereNot
    }
    if (whereNotOr) {
      result += result ? ' or ' + whereNotOr : whereNotOr
    }
    if (whereExclude) {
      if (result) {
        result = '(' + result + ') and ' + whereExclude
      }
      else {
        result = whereExclude
      }
    }
    return result ? '(' + result + ')' : ''
  }

  addBetweenStr (column, val1, val2, pad=0) {
    let c = []
    let lColumn = this._ignoreCase ? `upper(${column}})` : column
    if (val1) {
      c.push(`${lColumn} >= ${this._addParam(val1.padStart(pad), 's')}`)
    }
    if (val2) {
      c.push(`${lColumn} <= ${this._addParam(val2.padStart(pad), 's')}`)
    }
    let result = c.join(' and ')
    if (result) this._wheres.push('(' + result + ')')
  }

  addBetweenNum (column, val1, val2) {
    let c = []
    if (!!val1 || val1 === 0) {
      c.push(`${column} >= ${this._addParam(val1, 'n')}`)
    }
    if (!!val2 || val2 === 0) {
      c.push(`${column} <= ${this._addParam(val2, 'n')}`)
    }
    let result = c.join(' and ')
    if (result) this._wheres.push('(' + result + ')')
  }

  addBetweenDat (column, val1, val2) {
    let c = []
    if (val1) {
      c.push(`${column} >= ${this._addParam(val1, 'd')}`)
    }
    if (val2) {
      c.push(`${column} <= ${this._addParam(val2, 'd')}`)
    }
    let result = c.join(' and ')
    if (result) this._wheres.push('(' + result + ')')
  }

  addCompareStr (column, operation, val, pad = 0) {
    let lColumn = this._ignoreCase ? `upper(${column}})` : column
    if (val) {
      this._wheres.push(`(${lColumn} ${operation} ${this._addParam(val.padStart(pad), 's')})`)
    }
  }

  addCompareNum (column, operation, val) {
    if (!!val || val === 0) {
      this._wheres.push(`(${column} ${operation} ${this._addParam(val, 'n')})`)
    }
  }

  addCompareDat (column, operation, val) {
    if (val) {
      this._wheres.push(`(${column} ${operation} ${this._addParam(val, 'd')})`)
    }
  }

  addEnumNum (column, val) {
    if (val && val.length) {
      if (val.length === 1) {
        this._wheres.push(`(${column} = ${this._addParam(val[0], 'n')})`)
      }
      else {
        this._wheres.push(`(${column} in (${val.join(', ')}))`)
      }
    }
  }

  addEnumStr (column, val, pad = 0) {
    if (val && val.length) {
      const sVals = val.map(a => `'${a.padStart(pad)}'`)
      if (sVals.length === 1) {
        this._wheres.push(`(${column} = ${this._addParam(val[0].padStart(pad), 's')})`)
      }
      else {
        this._wheres.push(`(${column} in (${sVals.join(', ')}))`)
      }
    }
  }

  addIsNull (column) {
    this._wheres.push(`(${column} is null)`)
  }

  addIsNotNull (column) {
    this._wheres.push(`(${column} is not null)`)
  }

}

module.exports = Conditions
